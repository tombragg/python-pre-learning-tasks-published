def calculator(a, b, operator):
    if operator == "+":
        return bin(a + b)
    elif operator == "-":
        return bin(a - b)
    elif operator == "*":
        return bin(a * b)
    elif operator == "/":
        return bin(a // b)
    else:
        return "Please enter a +, -, * or / as your operator"


def decimalToBinary(num):
    if num > 1:
        decimalToBinary(num // 2)
    print(num % 2, end="")


print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console