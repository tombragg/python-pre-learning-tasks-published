def factors(number):

    factor_list = []

    for potential_factor in list(range(number)):
        if number % (potential_factor + 1) == 0 and potential_factor + 1 != 1 and potential_factor + 1 != number:
            factor_list.append(potential_factor + 1)

    return factor_list


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
