def vowel_swapper(string):

    change = {"A": "4", "a": "4",
              "E": 3, "e": 3,
              "I": "!", "i": "!",
              "O": "000", "o": "ooo",
              "U": "|_|", "u": "|_|"}

    output_string = ""

    for letter in string:
        output_string = output_string + str(change.get(letter, letter))

    return output_string


print(vowel_swapper("aAa eEe iIi oOo uUu"))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World"))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "Ev3rything's Av/\!lable" to the console
